package com.besheater.training.javafundamentals.task1;

import java.io.PrintStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {
        startApp(new Scanner(System.in), System.out);
    }

    public static void startApp(Scanner scanner, PrintStream out) {
        String name = getName(scanner, out);
        out.printf("Greetings %s!\n", name);

        List<BigInteger> numbers;
        try {
            numbers = getNumbers(scanner, out);
        } catch (IllegalArgumentException ex) {
            out.println(ex.getMessage());
            return;
        }

        out.printf("The sum of your numbers = %d\n", calculateSum(numbers));
        out.printf("The product of your numbers = %d\n", calculateProduct(numbers));
        out.println("That's all. Bye!");
    }

    public static String getName(Scanner scanner, PrintStream out) {
        out.println("Hello stranger, please enter your name:");
        return scanner.nextLine();
    }

    public static List<BigInteger> getNumbers(Scanner scanner, PrintStream out) {
        out.println("Now enter some integer numbers separated by space: " +
                    "(e.g. 5 4 -16 9 0 258 -42)");
        String numbersStr = scanner.nextLine();
        try {
            return parseDecimalIntegerNumbers(numbersStr);
        } catch (IllegalArgumentException ex) {
            String message = "Some of your numbers are not valid. " +
                    "Please type more carefully next time. Bye!";
            throw new IllegalArgumentException(message, ex);
        }
    }

    public static List<BigInteger> parseDecimalIntegerNumbers(String numbersStr) {
        String[] numbers = numbersStr.trim().split("\\s+");
        return Arrays.stream(numbers).map(BigInteger::new)
                                     .collect(Collectors.toList());
    }

    public static BigInteger calculateSum(List<BigInteger> numbers) {
        return numbers.stream().reduce(BigInteger.ZERO, BigInteger::add);
    }

    public static BigInteger calculateProduct(List<BigInteger> numbers) {
        return numbers.stream().reduce(BigInteger.ONE, BigInteger::multiply);
    }
}