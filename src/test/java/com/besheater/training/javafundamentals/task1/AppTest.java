package com.besheater.training.javafundamentals.task1;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.math.BigInteger.valueOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AppTest {

    @Test
    public void parseIntegerNumbers_nullArguments_throwsException() {
        assertThrows(NullPointerException.class,
                () -> App.parseDecimalIntegerNumbers(null));
    }

    @Test
    public void parseIntegerNumbers_invalidInput_throwsException() {
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers(""));
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers(" "));
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers("4.2 598 148.3"));
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers("7fd8 63v d87"));
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers("67/8 487 3101"));
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers("10E2 1E3 74"));
    }

    @Test
    public void parseIntegerNumbers_validInput_returnsAllNumbers() {
        assertEquals(asList(15),
                App.parseDecimalIntegerNumbers("15"));
        assertEquals(asList(-3),
                App.parseDecimalIntegerNumbers("-3"));
        assertEquals(asList(14, 25, 0, 789, 1),
                App.parseDecimalIntegerNumbers("014 25 0 789 1"));
        assertEquals(asList(-784, 13, 0, -1),
                App.parseDecimalIntegerNumbers(" -784 13 -0 -1"));
        assertEquals(asList(1, 2, 89, -3),
                App.parseDecimalIntegerNumbers(" 1  2 89    -3  "));
    }

    @Test
    public void calculateSum_nullArguments_throwsException() {
        assertThrows(NullPointerException.class, () -> App.calculateSum(null));
    }

    @Test
    public void calculateSum_nullValuesInCollection_throwsException() {
        assertThrows(NullPointerException.class,
                () -> App.calculateSum(asList(null)));
        assertThrows(NullPointerException.class,
                () -> App.calculateSum(asList(1, 0, null, -74)));
    }

    @Test
    public void calculateSum_validInput_returnSum() {
        assertEquals(valueOf(9), App.calculateSum(asList(9)));
        assertEquals(valueOf(10), App.calculateSum(asList(1, 2, 7)));
        assertEquals(valueOf(100), App.calculateSum(asList(20, 30, 50)));
        assertEquals(valueOf(0), App.calculateSum(asList(13, -13, 0)));
        assertEquals(valueOf(-17), App.calculateSum(asList(20, -20, -17)));
    }

    @Test
    public void calculateProduct_nullArguments_throwsException() {
        assertThrows(NullPointerException.class, () -> App.calculateProduct(null));
    }

    @Test
    public void calculateProduct_nullValuesInCollection_throwsException() {
        assertThrows(NullPointerException.class,
                () -> App.calculateProduct(asList(null)));
        assertThrows(NullPointerException.class,
                () -> App.calculateProduct(asList(-78, null, 0, 3)));
    }

    @Test
    public void calculateProduct_validInput_returnProduct() {
        assertEquals(valueOf(59), App.calculateProduct(asList(59)));
        assertEquals(valueOf(8), App.calculateProduct(asList(2, 2, 2)));
        assertEquals(valueOf(300), App.calculateProduct(asList(3, 10, 10)));
        assertEquals(valueOf(0), App.calculateProduct(asList(0, 17, 315487)));
        assertEquals(valueOf(-10), App.calculateProduct(asList(2, 5, -1)));
    }

    private static List<BigInteger> asList(Integer ... nums) {
        return Arrays.stream(nums).map(BigInteger::valueOf)
                                  .collect(Collectors.toList());
    }
}